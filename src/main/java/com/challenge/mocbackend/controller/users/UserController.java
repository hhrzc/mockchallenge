package com.challenge.mocbackend.controller.users;

import com.challenge.mocbackend.model.domain.User;
import com.challenge.mocbackend.model.domain.UserRole;
import com.challenge.mocbackend.model.enums.Roles;
import com.challenge.mocbackend.model.enums.Status;
import com.challenge.mocbackend.model.repo.RoleRepository;
import com.challenge.mocbackend.model.service.UserService;
import com.challenge.mocbackend.model.service.UserServiceImplementation;
import com.challenge.mocbackend.security.jwt.JwtTokenProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

@RestController
public class UserController {

    @Autowired
    private JwtTokenProvider jwtTokenProvider;

    @Autowired
    private UserService userService;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private UserServiceImplementation userServiceImplementation;

    @GetMapping("/user")
    public String test(){

        System.out.println("TTTTTTTTTTTTTTTTTTTTTTTTTTTTTEST");

        User localUser = new User();
        String encPass = jwtTokenProvider.passwordEncoder().encode("test");
        localUser.setPassword(encPass);
        localUser.setUserName("root1");
        localUser.setCreated(new Date());
        localUser.setUpdated(new Date());
        localUser.setStatus(Status.ACTIVE);
        Set<UserRole> userRoles = new HashSet<>();
        userRoles.add(roleRepository.getRoleByName(Roles.ROLE_USER.name()));
        userRoles.add(roleRepository.getRoleByName(Roles.ROLE_ADMIN.name()));
        System.out.println(Roles.ROLE_USER.name());

        localUser.setRoles(userRoles);
        userServiceImplementation.saveUser(localUser);
        return "Ok";
    }
}

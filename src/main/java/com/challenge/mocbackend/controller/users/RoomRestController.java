package com.challenge.mocbackend.controller.users;

import com.challenge.mocbackend.dto.requests.CreateRoomRequestDto;
import com.challenge.mocbackend.dto.response.ResultTextResponseDto;
import com.challenge.mocbackend.dto.response.RoomResponseDto;
import com.challenge.mocbackend.dto.requests.UserIdRequestDto;
import com.challenge.mocbackend.model.domain.Room;
import com.challenge.mocbackend.model.domain.User;
import com.challenge.mocbackend.model.service.RoomService;
import com.challenge.mocbackend.model.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static com.challenge.mocbackend.controller.users.ContentResponse.NO_CONTENT_RESPONSE;

@RestController
@RequestMapping
public class RoomRestController {

    @Autowired
    private RoomService roomService;

    @Autowired
    private UserService userService;

    @GetMapping(value = "rooms")
    public ResponseEntity<List<RoomResponseDto>> getAllRooms(){

        Set<Room> roomList = roomService.getAll();

        if (roomList == null){
            return NO_CONTENT_RESPONSE;
        }

        List<RoomResponseDto> result = roomList.stream()
                .map(RoomResponseDto::toDto)
                .collect(Collectors.toList());

        return new ResponseEntity<>(result, HttpStatus.OK);

    }

    @GetMapping(value = "rooms/{id}")
    public ResponseEntity<RoomResponseDto> getUserById(
            @PathVariable(name = "id") String roomId){

        Room room = roomService.findById(roomId);

        if (room == null){
            return NO_CONTENT_RESPONSE;
        }

        return getRoomDtoResponseEntity(room);
    }

    @DeleteMapping(value = "rooms/{id}")
    public ResponseEntity<ResultTextResponseDto> deleteRoom(
            @PathVariable(name = "id") String roomId,
            @RequestBody UserIdRequestDto requestDto
    ){
        if (roomService.validateRoomCreator(requestDto.getUser_id(), roomId));
        boolean delete = roomService.deleteById(roomId);

        if (!delete){
            return NO_CONTENT_RESPONSE;
        }

        return new ResponseEntity<>(ResultTextResponseDto.body("room removed successfully"),
                HttpStatus.OK);
    }


    @PostMapping(value = "rooms")
    public ResponseEntity<RoomResponseDto> createRoom(
            @RequestBody CreateRoomRequestDto requestDto
    ){
        Room room = roomService.createAndSaveRoom(requestDto.getName(), requestDto.getUser_id());

        if (room == null){
            return NO_CONTENT_RESPONSE;
        }
        return getRoomDtoResponseEntity(room);
    }

    @PostMapping(value = "rooms/{roomId}/join")
    public ResponseEntity<ResultTextResponseDto> joinUserIntoRoom(
            @PathVariable(name = "roomId") String roomId,
            @RequestBody UserIdRequestDto userIdDto
    ){

        Room room = roomService.findById(roomId);
        User user = userService.findById(userIdDto.getUser_id());
        user.getRooms().add(room);
        room.getUsers().add(user);
        return saveRoomAndGetResult(room, "user successfully joined the room");
    }

    @PostMapping(value = "rooms/{roomId}/leave")
    public ResponseEntity<ResultTextResponseDto> leaveUserFromRoom(
            @PathVariable(name = "roomId") String roomId,
            @RequestBody UserIdRequestDto userIdDto
    ){
        Room room = roomService.findById(roomId);
        User user = userService.findById(userIdDto.getUser_id());

//        user.getRooms().stream()
//                .map(r -> r.getId())
//                .forEach(System.out::println);
        User userToDelete = room.getUsers().stream()
                .filter(r -> r.getId().equals(userIdDto.getUser_id()))
                .findFirst().orElse(null);

        Room roomToDelete = user.getRooms().stream()
                .filter((r -> r.getId().equals(roomId)))
                .findFirst().orElse(null);

//        room.getUsers().stream()
//                .filter(r -> r.getId().equals(user.getId()))
//                .forEach(r -> System.out.println(r.equals(user)));


        room.getUsers().remove(userToDelete);
        user.getRooms().remove(roomToDelete);
//        user.getRooms().stream()
//                .map(r -> r.getId())
//                .forEach(System.out::println);
        room.getUsers().remove(user);

        userService.saveUser(user);
        room = roomService.saveRoom(room);

        return saveRoomAndGetResult(room, "user successfully left the room");
    }

    private ResponseEntity<ResultTextResponseDto> saveRoomAndGetResult(Object o, String message) {
        if (o == null){
            return NO_CONTENT_RESPONSE;
        }

        ResultTextResponseDto result = new ResultTextResponseDto();
        result.setResult(message);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }


    private ResponseEntity<RoomResponseDto> getRoomDtoResponseEntity(Room room) {
        RoomResponseDto result = RoomResponseDto.toDto(room);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }
}

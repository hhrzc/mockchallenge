package com.challenge.mocbackend.controller.users;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class ContentResponse {
    public static final ResponseEntity NO_CONTENT_RESPONSE = new ResponseEntity(HttpStatus.NO_CONTENT);
}

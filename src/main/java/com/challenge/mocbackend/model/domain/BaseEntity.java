package com.challenge.mocbackend.model.domain;

import com.challenge.mocbackend.model.enums.Status;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.util.Date;

@MappedSuperclass
public class BaseEntity {

    @CreatedDate
    @Column(name = "created")
    private Date created;

    @LastModifiedDate
    @Column(name = "updated")
    private Date updated;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private Status status;

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date initCreated) {
        created = initCreated;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date initUpdated) {
        updated = initUpdated;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status initStatus) {
        status = initStatus;
    }
}

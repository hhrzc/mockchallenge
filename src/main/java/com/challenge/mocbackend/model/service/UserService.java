package com.challenge.mocbackend.model.service;

import com.challenge.mocbackend.model.domain.Room;
import com.challenge.mocbackend.model.domain.User;

import java.util.List;
import java.util.Set;

public interface UserService {

    void saveUser(User user);
    User register(User user);
    Set<User> getAllUsers();
    User findById(String userId);
    User findByUserName(String username);

}

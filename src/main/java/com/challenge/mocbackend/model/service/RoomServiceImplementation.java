package com.challenge.mocbackend.model.service;

import com.challenge.mocbackend.model.domain.Room;
import com.challenge.mocbackend.model.domain.User;
import com.challenge.mocbackend.model.repo.RoomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
public class RoomServiceImplementation implements RoomService{

    @Autowired
    private UserService userService;

    @Autowired
    private RoomRepository roomRepository;

    @Override
    public Set<Room> getAll() {
        return new HashSet<>(roomRepository.findAll());
    }

    @Override
    public Room findById(String roomId) {
        Room room = roomRepository.findById(roomId).orElse(null);
        if (room == null){
            throw new NullPointerException("room with id " + roomId + " not found");
        }
        return room;
    }

    @Override
    public Room createAndSaveRoom(String roomName, String userId) {
        User creator = userService.findById(userId);

        Room room = new Room();
        room.setUsers(null);
        room.setCreateDate(new Date());
        room.setRoomName(roomName);
        room.setCreator(creator);

        room = roomRepository.save(room);

        return room;
    }

    @Override
    public boolean validateRoomCreator(String user_id, String roomId) {
        userService.findById(user_id); //throws NullPointerException if user not found;
        Room room = roomRepository.findById(roomId).orElse(null);
        if (room == null){
            throw new NullPointerException("Room with id " + roomId + " not found");
        }

        return room.getCreator().getId().equals(user_id);
    }

    @Override
    public boolean deleteById(String roomId) {
        Room room = findById(roomId);

        if (room == null){
            return false;
        }
        roomRepository.delete(room);
        return true;
    }

    @Transactional
    @Override
    public Room saveRoom(Room room) {
        Room result = roomRepository.save(room);
        return result;
    }
}

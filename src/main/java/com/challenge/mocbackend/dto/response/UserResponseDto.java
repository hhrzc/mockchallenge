package com.challenge.mocbackend.dto.response;

import com.challenge.mocbackend.model.domain.User;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UserResponseDto {

    private String id;
    private String username;

    public User toUser(){
        User user = new User();
        user.setId(this.id);
        user.setUserName(this.username);
        return user;
    }

    public static UserResponseDto fromUser (User user){
        UserResponseDto result = new UserResponseDto();
        result.setId(user.getId());
        result.setUsername(user.getUserName());
        return result;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}

package com.challenge.mocbackend.security.jwt;

import com.challenge.mocbackend.model.domain.User;
import com.challenge.mocbackend.model.domain.UserRole;
import com.challenge.mocbackend.model.enums.Status;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public final class JwtUserFactory {

    public JwtUserFactory(){

    }

    public static JwtUser create(User user){
        return new JwtUser(
                user.getId(),
                user.getUserName(),
                user.getPassword(),
                user.getUpdated(),
                mapToGrantedAuthority(new ArrayList<>(user.getRoles())),
                user.getStatus().equals(Status.ACTIVE)
        );
    }

    private static List<GrantedAuthority> mapToGrantedAuthority(List<UserRole> userRoles){
        return userRoles.stream()
                .map(r ->
                    new SimpleGrantedAuthority(r.getRoleName())
                )
                .collect(Collectors.toList());
    }


}

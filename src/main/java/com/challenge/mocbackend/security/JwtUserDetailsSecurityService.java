package com.challenge.mocbackend.security;

import com.challenge.mocbackend.model.domain.User;
import com.challenge.mocbackend.model.service.UserService;
import com.challenge.mocbackend.security.jwt.JwtUser;
import com.challenge.mocbackend.security.jwt.JwtUserFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class JwtUserDetailsSecurityService implements UserDetailsService {

    private final UserService userService;


    @Autowired
    public JwtUserDetailsSecurityService(UserService initUserService) {
        userService = initUserService;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userService.findByUserName(username);
        if (user == null){
            throw new UsernameNotFoundException("User with id " + username + " not found");
        }

        JwtUser jwtUser = JwtUserFactory.create(user);
        return jwtUser;
    }
}
